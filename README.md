# Usage

```bash
docker run -it --rm -v $(pwd):/app kalmac/gtfs-transformer-cli --transform=modifications.txt source-gtfs.zip target-gtfs.zip
```

All files in current directory are accessible to `gtfs-transformer-cli`.

To know more about `gtfs-transformer-cli`, see the
[documentation](http://developer.onebusaway.org/modules/onebusaway-gtfs-modules/1.3.4-SNAPSHOT/onebusaway-gtfs-transformer-cli.html)
